import { Toaster } from 'react-hot-toast'
import Home from './Home'
function App() {
	return (
		<div className='app'>
			<Toaster position='top-center' />
			<Home />
		</div>
	)
}

export default App
