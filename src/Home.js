import { useState } from "react"
import Modal from "./components/modal/Modal"
import AddObjective from "./components/AddObjective"
import AddConstraints from "./components/AddConstraints"
import "./Home.scss"
import toast from "react-hot-toast"
import SimplexAlgo from "./simplex/simplex"
import Constraints from "./components/Constraints"
import Objective from "./components/Objective"
import Tableaus from "./components/Tableaus"
import Dual from "./components/Dual"
import Solution from "./components/Solution"
import Help from "./components/modal/Help"
import { getTransposed } from "./simplex/helpers"
import Kanon from "./components/Kanon"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons"

const Home = () => {
  const [constraintsOpen, setConstraintsOpen] = useState(false)
  const [objectiveOpen, setObjectiveOpen] = useState(false)
  const [tableaus, setTableaus] = useState([])
  const [pivotCoords,setPivotCoords] = useState([])
  const [solution, setSolution] = useState({ coefficients: {} })
  const [objective, setObjective] = useState({})
  const [constraintArrays, setConstraintArrays] = useState([])
  const [helpOpen, setHelpOpen] = useState(false)
  const [constraints, setConstraints] = useState([])

  const addConstraint = (e) => {
    setConstraints([...constraints, e])
  }

  const updateObjective = (e) => {
    setObjective(e)
  }

  const deleteConstraint = (e) => {
    setConstraints(
      constraints.filter(
        (constraint) =>
          constraint.id !== Number(e.currentTarget.parentNode.dataset.id)
      )
    )
    toast.success("Ograničenje obrisano")
  }

  const deleteAll = () => {
    setObjective({})
    setConstraints([])
    setTableaus([])
    setSolution({ coefficients: {} })
    setConstraintArrays([])
    toast.success("Sve obrisano")
  }

  const onConstraint = () => {
    if (!objective.X1) {
      toast.error("Nije postavljena funkcija cilja")
      return null
    }
    setConstraintsOpen(true)
  }

  const simplexSolve = () => {
    if (!constraints.length) {
      toast.error("Nepotpun unos")
      return null
    }
    const solver = new SimplexAlgo({
      objective,
      constraints,
    })
    const result = solver.solve()
    if (!result.details.isOptimal) {
      toast.error("Rješenje nije optimalno")
      return null
    }
    setTableaus([...result.details.tableaus, result.details.finalTableau])
    setSolution(result.solution)
    setConstraintArrays(getTransposed(constraints))
    setPivotCoords(result.details.pivotCoords)
  }

  return (
    <>
      <div className="Wrapper">
        <div className="setup">
          <div className="infoWrap">
            <h2>Kontrole</h2>
            <button className="primary-btn" onClick={simplexSolve}>
              Riješi
            </button>
            <button className="danger-btn" onClick={deleteAll}>
              Obriši sve
            </button>
            <button className="icon" onClick={() => setHelpOpen(true)}>
              <FontAwesomeIcon icon={faQuestionCircle} size={"lg"} />
            </button>
          </div>
          <div className="infoWrap">
            <h2>Funkcija cilja</h2>
            <Objective objective={objective} />
            <div>
              <button
                className="primary-btn"
                onClick={() => setObjectiveOpen(true)}
              >
                Postavi funkciju cilja
              </button>
            </div>
          </div>

          <div className="infoWrap">
            <h2>Ograničenja</h2>
            <Constraints
              constraints={constraints}
              deleteConstraint={deleteConstraint}
            />

            <button className="primary-btn" onClick={onConstraint}>
              Dodaj ograničenje
            </button>
          </div>
          <Dual
            objective={objective}
            constraintArrays={constraintArrays}
            constraints={constraints}
          />
          <Kanon
            constraints={constraints}
            solution={solution}
            objective={objective}
            constraintArrays={constraintArrays}
          />
          <Solution solution={solution} tableaus={tableaus} />
        </div>

        <div className="result">
          <Tableaus
            objective={objective}
            tableaus={tableaus}
            constraints={constraints}
            pivotCoords = {pivotCoords}
          />
        </div>
      </div>

      <Modal isOpen={objectiveOpen} onClose={() => setObjectiveOpen(false)}>
        <AddObjective
          onClose={() => setObjectiveOpen(false)}
          updateObjective={updateObjective}
        />
      </Modal>

      <Modal isOpen={constraintsOpen} onClose={() => setConstraintsOpen(false)}>
        <AddConstraints
          addConstraint={addConstraint}
          onClose={() => setConstraintsOpen(false)}
          objective={objective}
        />
      </Modal>
      <Modal onClose={() => setHelpOpen(false)} isOpen={helpOpen}>
        <Help onClose={() => setHelpOpen(false)} />
      </Modal>
    </>
  )
}

export default Home
