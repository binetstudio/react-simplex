import { multiply } from 'mathjs'
import toast from 'react-hot-toast'
import {
	mapVector,
	constraintToPreRow,
	extractNamedSolution,
	generateTableauRow,
	generateObjectiveRow,
} from './helpers'
import simplexAlgo from './simplex-algo'

export default class SimplexAlgo {
	constructor({ objective, constraints }) {
		this.objective = objective
		this.constraints = constraints
		this.numRows = constraints.length + 1
		const { varNames, indicesToNames, namesToIndices, vectorValues } =
			mapVector(objective)
		this.varNames = varNames
		this.indicesToNames = indicesToNames
		this.namesToIndices = namesToIndices
		this.objectiveVectorValues = vectorValues
		this.tableau = this.generateTableau()
	}

	generateTableau() {
		const constraintPreRows = this.constraints.map(constraintToPreRow)
		const constraintTableauRows = constraintPreRows.map((preRow, index) =>
			generateTableauRow({
				...preRow,
				rowNo: index,
				numRows: this.numRows,
			})
		)
		// error located
		const objectiveTableauRow = generateObjectiveRow({
			vectorValues: multiply(this.objectiveVectorValues, -1),
			rowNo: this.numRows - 1,
			numRows: this.numRows,
		})
		const tableau = [...constraintTableauRows, objectiveTableauRow]
		return tableau
	}

	solve() {
		const details = simplexAlgo({
			tableau: this.tableau,
		})
		if (!details.isOptimal) {
			toast.error('Rješenje nije optimalno')
			return null
		}
		const solution = extractNamedSolution({
			tableau: details.finalTableau,
			indicesToNames: this.indicesToNames,
			allNames: this.varNames,
		})
		return {
			details,
			solution,
		}
	}
}
