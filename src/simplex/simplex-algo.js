import { getLastRow, applyPivoting, isAllNonNegative, getPivotCoords } from './helpers'

const isTableauOptimal = (tableau) => isAllNonNegative(getLastRow(tableau))

export default function simplexAlgo({ tableau, maxIterations = 50 }) {
	let curTableau = tableau
	let curIteration = 0
	const tableaus = []
	const pivotCoords = []
	while (!isTableauOptimal(curTableau) && curIteration < maxIterations) {
		tableaus.push(curTableau)
		const curPivotCoords = getPivotCoords(curTableau)
		pivotCoords.push(curPivotCoords)
		curTableau = applyPivoting(curTableau)
		curIteration += 1
	}
	return {
		finalTableau: curTableau,
		tableaus,
		isOptimal: isTableauOptimal(curTableau),
		pivotCoords
	}
}
