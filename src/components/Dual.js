const Dual = (props) => {
  const { objective, constraintArrays, constraints } = props
  if (!constraintArrays.length || !objective) return null

  return (
    <div className="infoWrap">
      <h2>Dualni oblik</h2>
      {constraintArrays.map((constraint, constraintIndex) => {
        return (
          <div key={Math.random()} className="bubble nonflex">
            {constraint.map((cell, index) => {
              return (
                <p key={Math.random()}>
                  {index && cell > 0 ? "+" : ""} {cell} Y <sub>{index + 1}</sub>
                </p>
              )
            })}
            <p>&ge; {objective[`X${constraintIndex + 1}`]}</p>
          </div>
        )
      })}
      <p>
        {constraintArrays[0].map((cell, index, arr) => {
          return (
            <span key={Math.random()}>
              Y<sub>{index + 1}</sub> {index + 1 === arr.length ? "" : " , "}
            </span>
          )
        })}
        &ge; 0
      </p>
      <p>
        Z <sup>d</sup> ={" "}
        {Object.values(constraints).map((value, index) => {
          return (
            <span key={Math.random()}>
              {index && value.constant > 0 ? " + " : ""} {value.constant} y
              <sub>{index + 1}</sub>
            </span>
          )
        })}
        &#8594; min
      </p>
    </div>
  )
}

export default Dual
