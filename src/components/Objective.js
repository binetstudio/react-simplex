const Objective = (props) => {
  const { objective } = props
  if (!objective.X1) return <p>Funkcija cilja nije postavljena</p>

  return (
    <>
    <span>Z =</span>
      {Object.keys(objective).map((key,index) => {
        return (
          <span key={Math.random()} className="funkcija">
            {index && objective[key] > 0 ? '+' : ''} {objective[key]} {key.slice(0, 1)}
            <sub>{key.slice(1)}</sub>
          </span>
        )
      })}
      <span> &#8594; max</span>
    </>
  )
}

export default Objective
