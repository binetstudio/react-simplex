import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faTrash } from "@fortawesome/free-solid-svg-icons"
const Constraints = (props) => {
  const { constraints } = props
  if (!constraints.length) return <p>Ograničenja nisu postavljena</p>

  return (
    <>
      {constraints.map((constraint) => {
        return (
          <div key={constraint.id} className="bubble">
            <div>
              {Object.keys(constraint.namedVector).map((key, index) => {
                return (
                  <span key={Math.random()}>
                    <span>
                      {index && constraint.namedVector[key] > 0 ? "+" : ""}
                    </span>{" "}
                    {constraint.namedVector[key]} {key.slice(0, 1)}{" "}
                    <sub>{key.slice(1)} </sub>
                  </span>
                )
              })}
              <span>&le; {constraint.constant}</span>
            </div>
            <span data-id={constraint.id}>
              <FontAwesomeIcon
                icon={faTrash}
                className="icon"
                onClick={props.deleteConstraint}
                size={"lg"}
              />
            </span>
          </div>
        )
      })}
      <p>
        {Object.keys(constraints[0]?.namedVector).map((key, index, arr) => {
          return (
            <span key={Math.random()}>
              X <sub>{index + 1}</sub>
              {index + 1 === arr.length ? "" : " , "}
            </span>
          )
        })}
        &ge; 0
      </p>
    </>
  )
}

export default Constraints
