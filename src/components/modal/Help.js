const Help = props => {
    const {onClose} = props
    return <>
    <h1>O aplikaciji</h1>
    <h2>Što ova aplikacija radi?</h2>
    <p>
        Ova aplikacija rješava standardni problem linearnog programiranja za maksimum koristeći metodu dvofaznog simpleksa.
        
    </p>
    <h2>Kako koristiti ovu aplikaciju?</h2>
        <p>
            Potrebno je prvo postaviti funkciju cilja, a zatim postaviti ograničenja.
        </p>
        <button onClick={onClose} className="primary-btn">
            U redu
        </button>
    </>
}

export default Help