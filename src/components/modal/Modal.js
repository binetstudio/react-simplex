import React, { useEffect } from 'react'
import reactDom from 'react-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { motion, AnimatePresence } from 'framer-motion'
import './Modal.scss'

const Modal = (props) => {
	useEffect(() => {
		const close = (e) => (e.key === 'Escape' ? props.onClose() : null)

		window.addEventListener('keydown', close)
		return () => window.removeEventListener('keydown', close)
	})

	return reactDom.createPortal(
		<AnimatePresence exitBeforeEnter>
			{props.isOpen && (
				<div className='overlay'>
					<motion.div
						initial={{ scale: 0 }}
						animate={{ scale: 1 }}
						exit={{ scale: 0 }}>
						<div className='modalCard'>
							<div className='innerContent'>
								<FontAwesomeIcon
									className='close'
									icon={faTimesCircle}
									size={'lg'}
									onClick={props.onClose}
								/>
								{props.children}
							</div>
						</div>
					</motion.div>
				</div>
			)}
		</AnimatePresence>,
		document.getElementById('portal')
	)
}

export default Modal
