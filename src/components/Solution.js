import { toFraction } from "../simplex/helpers"
const Solution = (props) => {
  const { solution, tableaus } = props
  if (!tableaus.length || !solution) return null

  return (
    <div className="infoWrap">
      <h2>Rješenje</h2>

      {Object.keys(solution.coefficients).map((coef) => {
        return (
          <div key={Math.random()}>
            <b>{coef} =</b> {toFraction(solution.coefficients[coef])}
          </div>
        )
      })}
      <div>
        <p>
          <b>Z =</b> {toFraction(solution?.optimum)}
        </p>
      </div>
    </div>
  )
}

export default Solution
