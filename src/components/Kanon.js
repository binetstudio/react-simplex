import { abs } from "mathjs"
import { mapVector } from "../simplex/helpers"

const Kanon = (props) => {
  const { constraints, constraintArrays, objective } = props
  if (!constraintArrays.length) return null
  let arrays = []
  constraints.forEach((constraint) => {
    arrays.push(mapVector(constraint.namedVector).vectorValues)
  })

  return (
    <div className="infoWrap">
      <h2>Kanonski oblik</h2>
      {arrays.map((constraint, constraintIndex) => {
        return (
          <div className="bubble nonflex" key={Math.random()}>
            {constraint.map((cell, index) => {
              return (
                <p key={Math.random()}>
                  {index && cell > 0 ? "+" : ""} {cell}X<sub>{index + 1}</sub>
                </p>
              )
            })}
            <span>
              + u<sub>{constraintIndex + 1}</sub> ={" "}
              {constraints[constraintIndex].constant}
            </span>
          </div>
        )
      })}
      <p>
        {Object.keys(constraints[0].namedVector).map((key, index) => {
          return (
            <span key={Math.random()}>
              X<sub>{index + 1}</sub>
              {" , "}
            </span>
          )
        })}
        {Object.keys(constraints[0].namedVector).map((key, index, arr) => {
          return (
            <span key={Math.random()}>
              u<sub>{index + 1}</sub> {index + 1 === arr.length ? "" : " , "}
            </span>
          )
        })}
        &ge; 0
      </p>

      <p>
        Z{" "}
        {Object.values(objective).map((value, index) => {
          return (
            <span key={Math.random()}>
              {value > 0 ? " - " : " + "} {value > 0 ? value : abs(value)} X<sub>{index + 1}</sub>
            </span>
          )
        })}{" "}
        - 0 &#8729; ({" "}
        {Object.values(constraints).map((value, index) => {
          return (
            <span key={Math.random()}>
              {index ? " + " : ""} u<sub>{index + 1}</sub>
            </span>
          )
        })}{" "}
        ) = 0
      </p>
    </div>
  )
}

export default Kanon
