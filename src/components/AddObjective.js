import { useState, useRef } from 'react'
import toast from 'react-hot-toast'
import './AddObjective.scss'

const AddObjective = (props) => {
	const numbers = useRef({})

	const onNumberEnter = (e) => {
		numbers.current = {
			...numbers.current,
			[`X${e.target.dataset.number}`]: Number(e.target.value),
		}
	}

	const [inputs, setInputs] = useState([
		<div key={Math.random()} className='objInput'>
			<input
				required
				defaultValue={0}
				onChange={onNumberEnter}
				data-number='1'
				type='number'
			/>
			<label>
				X<sub>1</sub>
			</label>
		</div>,
	])

	const addInput = () => {
		setInputs([
			...inputs,
			<div key={Math.random()} className='objInput'>
				<input
					required
					defaultValue={0}
					onChange={onNumberEnter}
					type='number'
					data-number={`${inputs.length + 1}`}
				/>
				<label>
					X<sub>{inputs.length + 1}</sub>
				</label>
			</div>,
		])
	}

	const onPostavi = (e) => {
		e.preventDefault()
		props.updateObjective(numbers.current)
		props.onClose()
		toast.success('Funkcija cilja postavljena')
	}

	return (
		<>
			<div className='wrap'>
				<form onSubmit={onPostavi}>
					{inputs.map((input) => input)}
					<button className='primary-btn' type='submit'>
						Postavi
					</button>
					<button className='secondary-btn' type='button' onClick={addInput}>
						Dodaj varijablu
					</button>
				</form>
			</div>
		</>
	)
}

export default AddObjective
