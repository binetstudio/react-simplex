import { useRef } from "react"
import toast from "react-hot-toast"

const AddConstraints = (props) => {
  const namedVector = useRef({})
  const { objective } = props
  const constant = useRef()

  const onNumberEnter = (e) => {
    namedVector.current = {
      ...namedVector.current,
      [`X${e.target.dataset.number}`]: Number(e.target.value),
    }
  }

  const onConstant = (e) => {
    constant.current = Number(e.target.value)
  }

  const onSubmit = (e) => {
    e.preventDefault()
    const constraint = {
      namedVector: namedVector.current,
      constant: constant.current,
      id: Math.random(),
    }
    props.addConstraint(constraint)
    toast.success("Ograničenje dodano")
    props.onClose()
  }

  return (
    <form onSubmit={onSubmit}>
      {Object.keys(objective).map((key) => {
        return (
          <div key={Math.random()}>
            <input
              required
              onChange={onNumberEnter}
              type="number"
              data-number={key.slice(1)}
            />
            <label>
              {" "}
              {key.slice(0, 1)}
              <sub>{key.slice(1)}</sub>{" "}
            </label>
            <span>+</span>
          </div>
        )
      })}
      <span>&le;</span> <input required onChange={onConstant} type="number" />
      <div>
        <button className="primary-btn" type="submit">
          Dodaj
        </button>
        <button className="secondary-btn" type="button" onClick={props.onClose}>
          Odustani
        </button>
      </div>
    </form>
  )
}

export default AddConstraints
