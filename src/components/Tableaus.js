import { toFraction } from "../simplex/helpers"
const Tableaus = (props) => {
  const { objective, tableaus, constraints, pivotCoords } = props
  if (!tableaus.length) return null
  return (
    <>
      {tableaus.map((tableau, tableauIndex) => {
        const pivot = pivotCoords[tableauIndex]
        return (
          <div key={Math.random()} className="infoWrap">
            <h3>Tablica {tableauIndex + 1}</h3>
            <table>
              <thead>
                <tr>
                  {Object.keys(objective).map((key) => (
                    <th key={Math.random()}>
                      {key.slice(0, 1)}
                      <sub>{key.slice(1)}</sub>
                    </th>
                  ))}
                  {constraints.map((constraint) => {
                    return (
                      <th key={Math.random()}>
                        u<sub>{constraints.indexOf(constraint) + 1}</sub>
                      </th>
                    )
                  })}
                  <th>Kol</th>
                </tr>
              </thead>
              <tbody>
                {tableau.map((row, rowIndex) => {
                  return (
                    <tr key={Math.random()}>
                      {row.map((cell, cellIndex) => {
                        return (
                          <td
                            style={
                              rowIndex === pivot?.rowNo &&
                              cellIndex === pivot?.colNo
                                ? { color: "white", backgroundColor: "#ce3838" }
                                : {}
                            }
                            key={Math.random()}
                          >
                            {toFraction(cell)}
                          </td>
                        )
                      })}
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        )
      })}
    </>
  )
}

export default Tableaus
